if status is-interactive
    # Commands to run in interactive sessions can go here
end

starship init fish | source
enable_transience

zoxide init fish | source

# bun
set --export BUN_INSTALL "$HOME/.bun"
set --export PATH $BUN_INSTALL/bin $PATH

# Abbreviations
abbr -a -- ll 'eza --long --icons=always'
abbr -a -- la 'eza --all --long --icons=always'
abbr -a -- ls 'eza --icons=always'
abbr -a -- lg 'eza --long --icons=always --git'
abbr -a -- looking-glass-client 'env -u WAYLAND_DISPLAY looking-glass-client -F -m KEY_RIGHTCTRL'

# Environment Variables
set -gx SHELL fish
set -gx ALTERNATE_EDITOR " "
set -gx EDITOR nvim
set -gx VISUAL "emacsclient -c -a emacs"
