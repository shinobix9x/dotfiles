function sshgemini --wraps='kitty +kitten ssh dimic@192.168.10.252' --description 'alias sshgemini=kitty +kitten ssh dimic@192.168.10.252'
  kitty +kitten ssh dimic@192.168.10.252 $argv
        
end
