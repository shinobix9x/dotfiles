(defconst chezmoi-packages
  '(
    chezmoi)
  "The list of Lisp packages required by the chezmoi layer.")

(defun chezmoi/init-chezmoi ()
  (use-package chezmoi
    :defer t
    :init
    (spacemacs/declare-prefix "a c" "chezmoi")

    (spacemacs/set-leader-keys
      "a c s" #'chezmoi-write
      "a c g" #'chezmoi-magit-status
      "a c d" #'chezmoi-diff
      "a c e" #'chezmoi-ediff
      "a c f" #'chezmoi-find
      "a c i" #'chezmoi-write-files
      "a c o" #'chezmoi-open-other
      "a c t" #'chezmoi-template-buffer-display
      "a c c" #'chezmoi-mode)

    :config

    ;; Additional hooks and settings
    (setq chezmoi-template-display-p t)
    (require 'chezmoi-company)
    (add-hook 'chezmoi-mode-hook #'(lambda () (if chezmoi-mode
                                                  (add-to-list 'company-backends 'chezmoi-company-backend)
                                                (setq company-backends (delete 'chezmoi-company-backend company-backends)))))
    ;; Optional: Disable ligatures
    (add-hook 'chezmoi-mode-hook #'(lambda () (ligature-mode (if chezmoi-mode 0 1))))

    ;; Tangle files with chezmoi-write if org files are used
    (defun chezmoi-org-babel-tangle ()
      (when-let ((fle (chezmoi-target-file (buffer-file-name))))
        (chezmoi-write file)))
    (add-hook 'org-babel-post-tangle-hook #'chezmoi-org-babel-tangle)))
